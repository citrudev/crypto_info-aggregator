const puppeteer = require('puppeteer');
const website = 'https://tulip.garden/leverage';
const APIServer = 'http://3.88.116.37/api/v1'
const Hash = require('object-hash')
const axios = require('axios')
let dataHash = null;
let tempDataHash = null
let store = [];
// vuTgeYC8WVM3wxzjLAn7
(async () => {
  try {

    browser = await puppeteer.launch({
        headless: false,
        defaultViewport: null,
        args:['--start-maximized'],
    })

    while (true) {
          const page = (await browser.pages())[0]
          await page.goto(website, { waitUntil: "networkidle0", timeout: 120000})

          try {
              let vault = await page.$$eval('.leverage-farming-table__row', rows => {
                  return Array.from(rows, row => {
                    const columns = row.querySelectorAll('.leverage-farming-table__row-item__asset__text-name')
                    return Array.from(columns, column => column.innerText)
                  })
                })

                let vault2 = await page.$$eval('.leverage-farming-table__row', rows => {
                  return Array.from(rows, row => {
                    const columns = row.querySelectorAll('.vaults-table__row-item__asset__text-tvl')
                    return Array.from(columns, column => column.innerText)
                  })
                })

                let vault3 = await page.$$eval('.leverage-farming-table__row', rows => {
                  return Array.from(rows, row => {
                    const columns = row.querySelectorAll('.leverage-farming-table__row-item__asset__text-tvl')
                    return Array.from(columns, column => column.innerText)
                  })
                })

                let adjustedApy = await page.$$eval('.leverage-farming-table__row', rows => {
                  return Array.from(rows, row => {
                    const columns = row.querySelectorAll('.adjusted-apy')
                    return Array.from(columns, column => column.innerText)
                  })
                })

                let initialApy = await page.$$eval('.leverage-farming-table__row', rows => {
                  return Array.from(rows, row => {
                    const columns = row.querySelectorAll('.initial-apy')
                    return Array.from(columns, column => column.innerText)
                  })
                })

                let YieldFarming = await page.$$eval('.leverage-farming-table__row', rows => {
                  return Array.from(rows, row => {
                    const columns = row.querySelectorAll('.yield-line-value')
                    return Array.from(columns, column => column.innerText)
                  })
                })


                let borrowingAprArr = await page.$$eval('.leverage-farming-table__row', rows => {
                  return Array.from(rows, row => {
                    const columns = row.querySelectorAll('.dropdown-item')
                    return Array.from(columns, column => column.innerText)
                  })
                })

                let leverage = await page.$$eval('.leverage-farming-table__row', rows => {
                  return Array.from(rows, row => {
                    const columns = row.querySelectorAll('.leverage-farming-table__row-item__cell')
                    return Array.from(columns, column => column.innerText)
                  })
                })

                leverage = leverage.map(arr => arr.slice(-1)[0])
                borrowingAprArr = borrowingAprArr.map(arr => arr.join(' | '))

                
                let x  = 0;
                while (x < vault.length) {
                  store.push({
                    'source': website,
                    'asset': vault[x][0],
                    'vault':vault2[x][0],
                    'tvl':vault3[x][0].split(':')[1],
                    'adjustedApy':adjustedApy[x][0].replace('\n', ' '),
                    'initialApy':initialApy[x][0],
                    'yieldFarming':YieldFarming[x][0],
                    'tradingFees':YieldFarming[x][1],
                    'borrowingApr':borrowingAprArr[x],
                    'totalApr':YieldFarming[x][3],
                    'dailyApr':YieldFarming[x][4],
                    'leverage': leverage[x]
                  })
                  x++
                }

                tempDataHash = Hash(store)

                if (dataHash != tempDataHash) {
                  console.log('data has changed')
                  dataHash = tempDataHash
                  console.log(dataHash)
                 
                  try {
                    let response  = await axios({
                      method: "post",
                      url: "http://3.88.116.37/api/v1/add-asset",
                      data: store,
                    })
                    console.log(response.data)
                  } catch (e) {
                    console.log('failed to update database...')
                  }
                  
                } else {
                  console.log('data has not changed')
                }
                
          } catch (e) {
            console.log(e)
          } 
          await page.waitForTimeout(300000)
      } 
    await page.waitForTimeout(50000)

    } catch (e) {
        console.log(e)
    }
})()